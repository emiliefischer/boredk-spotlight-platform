# Brug en Node.js base image
FROM node:18-alpine

# Opret og sæt arbejdsbiblioteket
WORKDIR /src

# Kopier package.json og installér afhængigheder
COPY package.json ./
RUN npm install

# Kopier hele projektet til Docker containeren
COPY . ./

# Byg dit React-projekt
RUN npm run build

# Eksponér den port, som din app kører på
EXPOSE 3000

# Kør projektet
CMD ["npm", "start"]
