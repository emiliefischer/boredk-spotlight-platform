#!/bin/bash
case "$RTE" in 
    dev )
        echo "** Development mode."
        npm run lint
        npm run dev
        ;;
    test )
        echo "** Test mode."
        npm run lint
        npm test
        ;;
    prod )
        echo "** Production mode."
        ;;
esac 