const mysql = require("mysql2/promise"); // Use promise-based mysql2

// create a new MySQL connection pool
const pool = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "Rxw46xhx!",
  database: "BOREDK_Spotlight_Platform",
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

// Function to test the connection
const testConnection = async () => {
  try {
    const connection = await pool.getConnection();
    console.log("Connection to the database was successful!");
    connection.release();
  } catch (error) {
    console.error("Failed to connect to the database:", error.message);
  }
};

// Execute the connection test
testConnection();

// Function to execute queries
const executeQuery = async (query, params = []) => {
  try {
    const [rows] = await pool.execute(query, params);
    return rows;
  } catch (error) {
    console.error("Query execution failed:", error.message);
    throw error;
  }
};

module.exports = { executeQuery };
