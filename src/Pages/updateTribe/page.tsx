import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import useUpdateTribe from "../../Hooks/useUpdateTribe";
import PageInfo from "../../Components/pageInfo/page";
import Section from "../../Layouts/section/page";
import LinkBackbtn from "../../Components/Buttons/linkbackbtn/page";
import { IoCreateOutline } from "react-icons/io5";

const UpdateTribe = () => {
  const { itemId } = useParams<{ itemId: string }>();
  const [formData, setFormData] = useState({
    name: "",
    description: "",
  });
  const [fetchingError, setFetchingError] = useState<string | null>(null);
  const { updateTribe, loading, error, successMessage, resetSuccessMessage } =
    useUpdateTribe();

  useEffect(() => {
    const fetchTribe = async () => {
      try {
        if (itemId) {
          const response = await axios.get(
            `http://localhost:4000/api/singleTribeByIdRoute/${itemId}`
          );
          setFormData({
            name: response.data.tribe_name,
            description: response.data.tribe_describtion, //skift til p
          });
        }
      } catch (error) {
        setFetchingError("Failed to fetch tribe data.");
      }
    };
    fetchTribe();
  }, [itemId]);

  useEffect(() => {
    if (successMessage) {
      const timer = setTimeout(() => {
        resetSuccessMessage();
      }, 5000); // Clear success message after 5 seconds
      return () => clearTimeout(timer);
    }
  }, [successMessage, resetSuccessMessage]);

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { id, value } = e.target;
    setFormData((prevData) => ({
      ...prevData,
      [id]: value,
    }));
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    if (!itemId) {
      setFetchingError("Invalid tribe ID.");
      return;
    }

    await updateTribe(itemId, formData);
  };

  return (
    <div className="dark:bg-dbBackgroundColorDark dark:text-dbWhiteDark px-8 pt-4 pb-24">
      <LinkBackbtn linkTo="/admindashboard" content="Go back to Dashboard" />
      <PageInfo
        title="Update"
        secondTitle="Tribe"
        content="Fill out the form and click on the submit button to update the tribe in Danske Bank."
      />
      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <form className="w-1/2 px-8" onSubmit={handleSubmit}>
          <div className="flex flex-col">
            <label htmlFor="name" className="text-md italic">
              Tribe Name
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="name"
              placeholder="Name of the tribe"
              value={formData.name}
              onChange={handleChange}
              required
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="description" className="text-md italic">
              Tribe Description
            </label>
            <textarea
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="description"
              placeholder="Description of the tribe"
              value={formData.description}
              onChange={handleChange}
              required
            />
          </div>
          <button
            className="flex gap-2 px-5 py-2 mt-4 text-md transition ease-in-out rounded-full shadow-md text-dbSand bg-dbBrightBlue hover:bg-dbBrightBlueHover dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:hover:bg-dbLightBlue"
            type="submit"
            disabled={loading}
          >
            Update Tribe <IoCreateOutline />
          </button>
        </form>
      </Section>
      {loading && <p>Loading...</p>}
      {fetchingError && <p>Error: {fetchingError}</p>}
      {error && <p>Error: {error}</p>}
      {successMessage && (
        <p className="text-dbBlue dark:text-dbWhiteDark">
          Success: {successMessage}
        </p>
      )}
    </div>
  );
};

export default UpdateTribe;
