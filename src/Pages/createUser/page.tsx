import React, { useEffect, useState } from "react";
import PrimaryButton from "../../Components/Buttons/primaryBtn/page";
import SecondaryButton from "../../Components/Buttons/secondaryBtn/page";
import axios from "axios";
import useFetchSquads from "../../Hooks/useFetchSquads";
import useFetchRoles from "../../Hooks/useFetchRoles";
import useCreateUser from "../../Hooks/useCreateUser";
import PageInfo from "../../Components/pageInfo/page";
import { RxReset } from "react-icons/rx";
import { IoCreateOutline } from "react-icons/io5";
import LinkBackbtn from "../../Components/Buttons/linkbackbtn/page";

interface UserFormInput {
  user_username: string;
  user_first_name: string;
  user_last_name: string;
  user_email: string;
  user_password: string;
  confirm_password: string;
  user_isSuperiorAdmin: boolean;
  user_isSquadAdmin: boolean;
  user_isTribeAdmin: boolean;
  user_avatar: string;
  user_squad_fk: string;
  user_role_fk: string;
  user_tribelead_fk: string; // Default to empty
}

const CreateUserForm: React.FC = () => {
  const { createUser, loading, error: backendError } = useCreateUser();
  const { squads, loading: squadLoading, error: squadError } = useFetchSquads();
  const { roles, loading: roleLoading, error: roleError } = useFetchRoles();
  const [formData, setFormData] = useState<UserFormInput>({
    user_username: "",
    user_first_name: "",
    user_last_name: "",
    user_email: "",
    user_password: "",
    confirm_password: "",
    user_isSuperiorAdmin: false,
    user_isSquadAdmin: false,
    user_isTribeAdmin: false,
    user_avatar: "",
    user_squad_fk: "",
    user_role_fk: "",
    user_tribelead_fk: "no",
  });

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value, type } = e.target;

    if (type === "checkbox" && e.target instanceof HTMLInputElement) {
      const { checked } = e.target;

      // Update state for the clicked checkbox
      setFormData((prevFormData) => ({
        ...prevFormData,
        [name]: checked,
      }));

      // If the clicked checkbox is an admin type, uncheck the other admin checkboxes
      if (name === "user_isSuperiorAdmin" && checked) {
        setFormData((prevFormData) => ({
          ...prevFormData,
          user_isSquadAdmin: false,
          user_isTribeAdmin: false,
        }));
      } else if (name === "user_isSquadAdmin" && checked) {
        setFormData((prevFormData) => ({
          ...prevFormData,
          user_isSuperiorAdmin: false,
          user_isTribeAdmin: false,
        }));
      } else if (name === "user_isTribeAdmin" && checked) {
        setFormData((prevFormData) => ({
          ...prevFormData,
          user_isSuperiorAdmin: false,
          user_isSquadAdmin: false,
        }));
      }

      // If the clicked checkbox is Tribe Admin and it's unchecked, reset user_tribelead_fk
      if (name === "user_isTribeAdmin" && !checked) {
        setFormData((prevFormData) => ({
          ...prevFormData,
          user_tribelead_fk: "no",
        }));
      }
    } else {
      // For non-checkbox inputs, update state directly
      setFormData((prevFormData) => ({
        ...prevFormData,
        [name]: value,
      }));
    }
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    if (formData.user_password !== formData.confirm_password) {
      alert("Passwords do not match!");
      return;
    }

    try {
      const adminCheckboxes = [
        formData.user_isSuperiorAdmin,
        formData.user_isSquadAdmin,
        formData.user_isTribeAdmin,
      ];

      const checkedAdminCount = adminCheckboxes.filter(
        (checkbox) => checkbox
      ).length;

      if (checkedAdminCount > 1) {
        throw new Error("Please choose only one admin type.");
      }

      const formDataObj = new FormData();

      // Append each key-value pair from UserFormInput to FormData
      Object.entries(formData).forEach(([key, value]) => {
        formDataObj.append(key, value);
      });

      console.log("Form Data:", formDataObj);

      // Proceed with form submission using FormData
      const responseMessage = await createUser(formDataObj);
      alert(responseMessage);
    } catch (error) {
      console.error("There was an error creating the user!", error);
      alert("Error creating user");
    }
  };

  return (
    <div className="dark:bg-dbBackgroundColorDark dark:text-dbWhiteDark pb-10 px-8 pt-4">
      <LinkBackbtn linkTo="/admindashboard" content="Go back to Dashboard" />
      <PageInfo
        title="Create a new"
        secondTitle="User"
        content="Fill out the form and click on the submit button to create a new user."
      />
      <form onSubmit={handleSubmit} className="space-y-4 w-1/2 px-8">
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Username:</label>
          <input
            type="text"
            name="user_username"
            value={formData.user_username}
            onChange={handleChange}
            required
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          />
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">First Name:</label>
          <input
            type="text"
            name="user_first_name"
            value={formData.user_first_name}
            onChange={handleChange}
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          />
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Last Name:</label>
          <input
            type="text"
            name="user_last_name"
            value={formData.user_last_name}
            onChange={handleChange}
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          />
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Email:</label>
          <input
            type="email"
            name="user_email"
            value={formData.user_email}
            onChange={handleChange}
            required
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          />
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Password:</label>
          <input
            type="password"
            name="user_password"
            value={formData.user_password}
            onChange={handleChange}
            required
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          />
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Confirm Password:</label>
          <input
            type="password"
            name="confirm_password"
            value={formData.confirm_password}
            onChange={handleChange}
            required
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          />
        </div>
        <div>
          <p>
            Should the user have admin rights? <i>Choose only one</i>
          </p>
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">
            <input
              type="checkbox"
              name="user_isSuperiorAdmin"
              checked={formData.user_isSuperiorAdmin}
              onChange={handleChange}
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            />
            Superior Admin
          </label>
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">
            <input
              type="checkbox"
              name="user_isSquadAdmin"
              checked={formData.user_isSquadAdmin}
              onChange={handleChange}
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            />
            Squad Admin
          </label>
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">
            <input
              type="checkbox"
              name="user_isTribeAdmin"
              checked={formData.user_isTribeAdmin}
              onChange={handleChange}
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            />
            Tribe Admin
          </label>
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Avatar:</label>
          <input
            type="text"
            name="user_avatar"
            value={formData.user_avatar}
            onChange={handleChange}
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          />
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Squad FK:</label>
          <select
            name="user_squad_fk"
            value={formData.user_squad_fk}
            onChange={handleChange}
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          >
            <option value="">Select Squad</option>
            {squads.map((squad) => (
              <option key={squad.squad_id} value={squad.squad_id}>
                {squad.squad_name}
              </option>
            ))}
          </select>
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Role FK:</label>
          <select
            name="user_role_fk"
            value={formData.user_role_fk}
            onChange={handleChange}
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          >
            <option value="">Select Role</option>
            {roles.map((role) => (
              <option key={role.role_id} value={role.role_id}>
                {role.role_name}
              </option>
            ))}
          </select>
        </div>
        <div className="flex flex-col mt-4">
          <label className="text-md italic">Tribe Lead:</label>
          <select
            name="user_tribelead_fk"
            value={formData.user_tribelead_fk}
            onChange={handleChange}
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          >
            <option value="no">No</option>
            <option value="yes">Yes</option>
          </select>
        </div>
        <div>
          <button
            className="flex gap-2 px-5 py-2 my-4 text-md transition ease-in-out rounded-full shadow-md text-dbSand bg-dbBrightBlue hover:bg-dbBrightBlueHover dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:hover:bg-dbLightBlue"
            type="submit"
            disabled={loading}
          >
            Create User
            <IoCreateOutline />
          </button>
          <button
            onClick={() =>
              setFormData({
                user_username: "",
                user_first_name: "",
                user_last_name: "",
                user_email: "",
                user_password: "",
                confirm_password: "",
                user_isSuperiorAdmin: false,
                user_isSquadAdmin: false,
                user_isTribeAdmin: false,
                user_avatar: "",
                user_squad_fk: "",
                user_role_fk: "",
                user_tribelead_fk: "no",
              })
            }
            className="flex items-center gap-2 px-5 py-2 transition ease-in-out rounded-full shadow-md text-md w-fit text-dbBlue bg-dbLightBlue hover:bg-dbLightBlueHover dark:bg-dbLightBlueDark dark:hover:bg-dbWhiteDark"
          >
            {" "}
            Reset <RxReset />
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreateUserForm;
