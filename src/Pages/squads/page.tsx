import React from "react";
import SquadCard from "../../Components/Cards/squadCard/page";
import Section from "../../Layouts/section/page";
import PageInfo from "../../Components/pageInfo/page";
import PrimaryButton from "../../Components/Buttons/primaryBtn/page";
import InfoBoxSection from "../../Layouts/infoBox/page";
import SearchField from "../../Components/searchField/page";
import FilterField from "../../Components/filterField/page";
import useFetchSquads from "../../Hooks/useFetchSquads";

const AllSquads: React.FC = () => {
  const { squads, loading, error } = useFetchSquads(); // Use the hook

  return (
    <div className=" bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      <PageInfo
        title="About"
        secondTitle="Our Squads in Danske Bank"
        content="Welcome to the dynamic realm of Squads within Danske Bank! This is where collaboration meets innovation, and teams come together to tackle challenges and drive projects forward. Explore an array of Squads, each a powerhouse of talent and expertise, working towards common goals and driving impactful outcomes. From agile development teams to cross-functional units, this is your window into the vibrant world of Squads within Danske Bank."
      />
      <Section
        bgColorLightMode="bg-dbLightBlue"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            Overview of all Squads
          </h2>
          <div className="grid gap-5 pb-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <SearchField />
            <FilterField />
          </div>
          {error ? ( // Display error message if error exists
            <p className="text-red-500 text-center">{error}</p>
          ) : (
            <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
              {loading ? (
                <p>Loading...</p>
              ) : (
                squads.map((squad) => (
                  <SquadCard key={squad.squad_name} squad={squad} />
                ))
              )}
            </div>
          )}
          <div className="flex justify-center my-8">
            <PrimaryButton content="Load all Squads" linkTo="/squads" />
          </div>
        </div>
      </Section>
      <InfoBoxSection />
    </div>
  );
};

export default AllSquads;
