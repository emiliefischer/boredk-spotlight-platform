import React from "react";
import person_placeholder from "../../../Assets/Images/person_placeholder.svg";
import edit from "../../../Assets/Images/edit.svg";
import Section from "../../../Layouts/section/page";
import PrimaryButton from "../../../Components/Buttons/primaryBtn/page";
import InfoBoxSection from "../../../Layouts/infoBox/page";
import useFetchProducts from "../../../Hooks/useFetchProducts";
import useFetchCurrentUser from "../../../Hooks/useFetchCurrentUser";
import ProductCard from "../../../Components/Cards/productCard/page";
import { User } from "../../../Types/userInterface";

interface CurrentUserProps {
  user: User;
}

const UserProfile: React.FC<CurrentUserProps> = ({ user }) => {
  const {
    user: currentUserData,
    loading: usersLoading,
    error: usersError,
  } = useFetchCurrentUser(user.user_id);

  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts(4);

  return (
    <Section
      bgColorLightMode="bg-dbBackgroundColor"
      bgColorDarkMode="bg-dbBackgroundColorDark"
    >
      {usersLoading ? (
        <p>Loading...</p>
      ) : usersError ? (
        <p className="text-center text-red-500">{usersError}</p>
      ) : (
        <div className="p-8">
          <h1 className="mb-8 font-medium text-h1 dark:text-dbWhiteDark ">
            {user.user_first_name}, {user.user_last_name}
          </h1>
          <section className="flex items-center">
            <div className="mr-8">
              <img
                src={person_placeholder}
                alt="profile_picture"
                className="h-60"
              />
            </div>
            <ul className="flex flex-col flex-grow gap-3 text-dbBlue dark:text-dbWhite">
              <li className="flex items-center justify-between ">
                <div className="grid items-center grid-cols-2 gap-12">
                  <h2 className="font-bold ">Username</h2>
                  <span className="">{user.user_username}</span>
                </div>
                <button className="text-blue-500 hover:text-blue-700">
                  <img src={edit} alt="Edit" />
                </button>
              </li>
              <li className="flex items-center justify-between">
                <div className="grid items-center grid-cols-2 gap-12">
                  <h2 className="font-bold ">Password</h2>
                  <span className="">{user.user_password}</span>
                </div>
                <button className="text-blue-500 hover:text-blue-700">
                  <img src={edit} alt="Edit" />
                </button>
              </li>
              <li className="flex items-center justify-between">
                <div className="grid items-center grid-cols-2 gap-12">
                  <h2 className="font-bold ">Email</h2>
                  <span className="">{user.user_email}</span>
                </div>
                <button className="text-blue-500 hover:text-blue-700">
                  <img src={edit} alt="Edit" />
                </button>
              </li>
              <li className="flex items-center justify-between">
                <div className="grid items-center grid-cols-2 gap-12">
                  <h2 className="font-bold ">Role</h2>
                  <span className="">{user.user_role_fk}</span>
                </div>
                <button className="text-blue-500 hover:text-blue-700">
                  <img src={edit} alt="Edit" />
                </button>
              </li>
              <li className="flex items-center justify-between">
                <div className="grid items-center grid-cols-2 gap-12">
                  <h2 className="font-bold ">Part of Tribe</h2>
                  <span className="">username</span>
                </div>
                <button className="text-blue-500 hover:text-blue-700">
                  <img src={edit} alt="Edit" />
                </button>
              </li>
              <li className="flex items-center justify-between">
                <div className="grid items-center grid-cols-2 gap-12">
                  <h2 className="font-bold ">Part of Squad</h2>
                  <span className="">{user.user_squad_fk}</span>
                </div>
                <button className="text-blue-500 hover:text-blue-700">
                  <img src={edit} alt="Edit" />
                </button>
              </li>
            </ul>
          </section>
        </div>
      )}
      <Section
        bgColorLightMode="bg-dbLightBlue"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            Part of projects
          </h2>
          {/* Dynamisk indhold skal genereres her så den henter maksimalt de 4 nyeste projekter fra databasen */}
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="See all projects" linkTo="#" />
          </div>
        </div>
      </Section>
      <InfoBoxSection />
    </Section>
  );
};

export default UserProfile;
