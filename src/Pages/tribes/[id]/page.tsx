import React, { useEffect, useState } from "react";
import PageInfo from "../../../Components/pageInfo/page";
import Section from "../../../Layouts/section/page";
import LeaderCard from "../../../Components/Cards/leaderCard/page";
import person_placeholder from "../../../Assets/Images/person_placeholder.svg";
import SquadCard from "../../../Components/Cards/squadCard/page";
import SearchField from "../../../Components/searchField/page";
import PrimaryButton from "../../../Components/Buttons/primaryBtn/page";
import InfoBoxSection from "../../../Layouts/infoBox/page";
import { useParams } from "react-router-dom";
import { Tribe } from "../../../Types/tribeInterface";
import axios from "axios";
import useFetchSquads from "../../../Hooks/useFetchSquads";

const SingleTribe = () => {
  const { tribeName } = useParams<{ tribeName: string }>();
  const [tribe, setTribe] = useState<Tribe | null>(null);
  const {
    squads,
    loading: squadsLoading,
    error: squadsError,
  } = useFetchSquads(4);

  useEffect(() => {
    const fetchTribe = async () => {
      try {
        const response = await axios.get(
          `http://localhost:4000/api/tribes/${tribeName}`
        );
        setTribe(response.data);
      } catch (error) {
        console.error("Error fetching tribe:", error);
      }
    };

    fetchTribe();
  }, [tribeName]);

  if (!tribe) {
    return <div>Loading...</div>;
  }

  return (
    <div className=" bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      <PageInfo
        title="About"
        secondTitle={tribe.tribe_name}
        content={tribe.tribe_describtion}
      />
      <section className="py-12 mb-4 bg-dbBlue dark:bg-dbCardBackgroundColorDark">
        <h2 className="text-center text-h2 text-dbWhite">Tribe Lead Roles</h2>
        <div className="flex justify-between p-8">
          <LeaderCard
            image={person_placeholder}
            name="Julia Broflovski"
            role="Senior Developer"
          />
          <LeaderCard
            image={person_placeholder}
            name="Michael Thompson"
            role="Senior Developer"
          />
          <LeaderCard
            image={person_placeholder}
            name="Emily Johnson"
            role="Senior Developer"
          />
          <LeaderCard
            image={person_placeholder}
            name="David Carter"
            role="Senior Developer"
          />
        </div>
      </section>
      <Section
        bgColorLightMode="bg-dbLightBlue"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbSand">
            Overview of all Squads
          </h2>
          <div className="grid gap-5 pb-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <SearchField />
          </div>
          <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {squadsLoading ? (
              <p>Loading...</p>
            ) : squadsError ? (
              <p className="text-center text-red-500">{squadsError}</p>
            ) : (
              squads.map((squad) => (
                <SquadCard key={squad.squad_name} squad={squad} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="See all Squads" linkTo="#" />
          </div>
        </div>
      </Section>
      <InfoBoxSection />
    </div>
  );
};

export default SingleTribe;
