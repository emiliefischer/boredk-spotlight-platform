import React, { useState, useEffect, ChangeEvent, FormEvent } from "react";
import useCreateProduct from "../../Hooks/useCreateProducts";
import PageInfo from "../../Components/pageInfo/page";
import Section from "../../Layouts/section/page";
import LinkBackbtn from "../../Components/Buttons/linkbackbtn/page";
import useFetchUsers from "../../Hooks/useFetchUsers";
import useFetchSquads from "../../Hooks/useFetchSquads";
import useFetchProductStatuses from "../../Hooks/useFetchProductStatuses";
import { IoCreateOutline } from "react-icons/io5";

const CreateProduct: React.FC = () => {
  const [formData, setFormData] = useState({
    productTitle: "",
    productDescription: "",
    productImage: "",
    productGithubLink: "",
    productFigmaLink: "",
    productHostLink: "",
    productConfluenceLink: "",
    productSquadFk: "",
    productProductOwnerFk: "",
    productStatusFk: "",
  });
  const { createProduct, loading, error, successMessage, resetSuccessMessage } =
    useCreateProduct();
  const { users, loading: usersLoading, error: usersError } = useFetchUsers();
  const {
    squads,
    loading: squadsLoading,
    error: squadsError,
  } = useFetchSquads();
  const {
    statuses,
    loading: statusesLoading,
    error: statusesError,
  } = useFetchProductStatuses();

  useEffect(() => {
    if (successMessage) {
      const timer = setTimeout(() => {
        resetSuccessMessage();
      }, 5000); // Clear success message after 5 seconds
      return () => clearTimeout(timer);
    }
  }, [successMessage, resetSuccessMessage]);

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleTextareaChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      // Check if required fields are provided
      if (
        !formData.productTitle ||
        !formData.productDescription ||
        !formData.productSquadFk ||
        !formData.productProductOwnerFk ||
        !formData.productStatusFk
      ) {
        throw new Error("Please provide all required fields.");
      }

      await createProduct(formData);
      setFormData({
        productTitle: "",
        productDescription: "",
        productImage: "",
        productGithubLink: "",
        productFigmaLink: "",
        productHostLink: "",
        productConfluenceLink: "",
        productSquadFk: "",
        productProductOwnerFk: "",
        productStatusFk: "",
      });
    } catch (error) {
      console.error("Error creating product:", error);
    }
  };

  return (
    <div className="dark:bg-dbBackgroundColorDark dark:text-dbWhiteDark px-8 pt-4 pb-24">
      <LinkBackbtn linkTo="/admindashboard" content="Go back to Dashboard" />
      <PageInfo
        title="Create a new"
        secondTitle="Product"
        content="Fill out the form and click on the submit button to create a new product."
      />
      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <form className="w-1/2 px-8" onSubmit={handleSubmit}>
          <div className="flex flex-col">
            <label htmlFor="productTitle" className="text-md italic">
              Product Title
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="productTitle"
              name="productTitle"
              placeholder="Title of the product"
              value={formData.productTitle}
              onChange={handleInputChange}
              required
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productDescription" className="text-md italic">
              Product Description
            </label>
            <textarea
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="productDescription"
              name="productDescription"
              placeholder="Description of the product"
              value={formData.productDescription}
              onChange={handleTextareaChange}
              required
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productImage" className="text-md italic">
              Product Image URL
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="productImage"
              name="productImage"
              placeholder="Image URL of the product"
              value={formData.productImage}
              onChange={handleInputChange}
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productGithubLink" className="text-md italic">
              GitHub Link
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="productGithubLink"
              name="productGithubLink"
              placeholder="GitHub link of the product"
              value={formData.productGithubLink}
              onChange={handleInputChange}
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productFigmaLink" className="text-md italic">
              Figma Link
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="productFigmaLink"
              name="productFigmaLink"
              placeholder="Figma link of the product"
              value={formData.productFigmaLink}
              onChange={handleInputChange}
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productHostLink" className="text-md italic">
              Hosting Link
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="productHostLink"
              name="productHostLink"
              placeholder="Hosting link of the product"
              value={formData.productHostLink}
              onChange={handleInputChange}
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productConfluenceLink" className="text-md italic">
              Confluence Link
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="productConfluenceLink"
              name="productConfluenceLink"
              placeholder="Confluence link of the product"
              value={formData.productConfluenceLink}
              onChange={handleInputChange}
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productSquadFk" className="text-md italic">
              Squad
            </label>
            <select
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="productSquadFk"
              name="productSquadFk"
              value={formData.productSquadFk}
              onChange={handleInputChange}
              required
            >
              <option value="">Select a squad</option>
              {squads.map((squad) => (
                <option key={squad.squad_id} value={squad.squad_id}>
                  {squad.squad_name}
                </option>
              ))}
            </select>
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productProductOwnerFk" className="text-md italic">
              Product Owner
            </label>
            <select
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="productProductOwnerFk"
              name="productProductOwnerFk"
              value={formData.productProductOwnerFk}
              onChange={handleInputChange}
              required
            >
              <option value="">Select a product owner</option>
              {users.map((user) => (
                <option key={user.user_id} value={user.user_id}>
                  {user.user_first_name} {user.user_last_name}
                </option>
              ))}
            </select>
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="productStatusFk" className="text-md italic">
              Product Status
            </label>
            <select
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="productStatusFk"
              name="productStatusFk"
              value={formData.productStatusFk}
              onChange={handleInputChange}
              required
            >
              <option value="">Select a status</option>
              {statuses.map((status) => (
                <option key={status.status_id} value={status.status_id}>
                  {status.status_name}
                </option>
              ))}
            </select>
          </div>
          <button
            className="flex gap-2 px-5 py-2 mt-4 text-md transition ease-in-out rounded-full shadow-md text-dbSand bg-dbBrightBlue hover:bg-dbBrightBlueHover dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:hover:bg-dbLightBlue"
            type="submit"
            disabled={loading}
          >
            Create Product <IoCreateOutline />
          </button>
        </form>
      </Section>
      {loading && <p>Loading...</p>}
      {error && <p>Error: {error}</p>}
      {successMessage && (
        <p className="text-dbBlue dark:text-dbWhiteDark">
          Success: {successMessage}
        </p>
      )}
    </div>
  );
};

export default CreateProduct;
