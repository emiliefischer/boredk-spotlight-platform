import React from "react";
import PageInfo from "../../Components/pageInfo/page";
import StatisticsOverview from "../../Components/statisticsOverview/page";
import ProductCard from "../../Components/Cards/productCard/page";
import Section from "../../Layouts/section/page";
import SquadCard from "../../Components/Cards/squadCard/page";
import PrimaryButton from "../../Components/Buttons/primaryBtn/page";
import InfoBoxSection from "../../Layouts/infoBox/page";
import useAuth from "../../Hooks/useAuthHook";
import useFetchSquads from "../../Hooks/useFetchSquads";
import useFetchProducts from "../../Hooks/useFetchProducts";

const Home: React.FC = () => {
  const {
    squads,
    loading: squadsLoading,
    error: squadsError,
  } = useFetchSquads(4);
  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts(4);
  const loggedIn = useAuth();

  return (
    <div className="bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      {/* Vis besked hvis brugeren er logget ind */}
      {loggedIn && (
        <Section
          bgColorLightMode="bg-dbBackgroundColor"
          bgColorDarkMode="bg-dbCardBackgroundColorDark"
        >
          <div>
            <p className="text-center text-dbBlue dark:text-dbWhite">
              {/* Hej, {user.user_username}! */}
            </p>
          </div>
        </Section>
      )}
      <section>
        <PageInfo
          title="Welcome to"
          secondTitle="BOREDK Spotlight Platform"
          content="BOREDK Spotlight Platform is Danske Bank's new internal release hub. The platform is designed to be your central source for internal knowledge sharing. Here you can follow what is happening throughout Danske Bank, from product development to business analyses. Our goal is to create an open and informative platform that makes it easy for all employees to stay updated and collaborate across departments."
        />
      </section>
      <Section
        bgColorLightMode="bg-dbBlue"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <StatisticsOverview />
      </Section>
      <Section
        bgColorLightMode="bg-dbWhite"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            Newly released projects
          </h2>
          {/* Dynamisk indhold skal genereres her så den henter maksimalt de 4 nyeste projekter fra databasen */}
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="See all products" linkTo="/products" />
          </div>
        </div>
      </Section>
      <section>
        <div className="px-4 py-12 bg-dbLightBlue dark:bg-dbCardBackgroundColorDark">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbBlue dark:text-dbWhiteDark">
            All Squads in Danske Bank
          </h2>
          <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {squadsLoading ? (
              <p>Loading...</p>
            ) : squadsError ? (
              <p className="text-center text-red-500">{squadsError}</p>
            ) : (
              squads.map((squad) => (
                <SquadCard key={squad.squad_name} squad={squad} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="See all Squads" linkTo="/squads" />
          </div>
        </div>
      </section>
      <InfoBoxSection />
      <section className="mt-4 bg-dbBlue dark:bg-dbBackgroundColorDark">
        <div className="px-4 py-12">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbSand ">
            Latest updates
          </h2>
          {/* Dynamisk indhold skal genereres her så den henter maksimalt de 4 nyeste projekter fra databasen */}
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
        </div>
      </section>
    </div>
  );
};

export default Home;
