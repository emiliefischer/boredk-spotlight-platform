import React, { useState, useEffect } from "react";
import useCreateTribe from "../../Hooks/useCreateTribe";
import PageInfo from "../../Components/pageInfo/page";
import Section from "../../Layouts/section/page";
import LinkBackbtn from "../../Components/Buttons/linkbackbtn/page";
import { IoCreateOutline } from "react-icons/io5";

const CreateTribe: React.FC = () => {
  const [tribeName, setTribeName] = useState("");
  const [tribeDescription, setTribeDescription] = useState("");
  const { createTribe, loading, error, successMessage, resetSuccessMessage } =
    useCreateTribe();

  useEffect(() => {
    if (successMessage) {
      console.log("Success Message:", successMessage);
      const timer = setTimeout(() => {
        resetSuccessMessage();
      }, 5000); // Clear success message after 5 seconds
      return () => clearTimeout(timer);
    }
  }, [successMessage, resetSuccessMessage]);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      console.log("Submitting form...");
      console.log("Tribe Name:", tribeName);
      console.log("Tribe Description:", tribeDescription);

      if (!tribeName || !tribeDescription) {
        throw new Error("Please provide both tribe name and description.");
      }

      await createTribe({ name: tribeName, description: tribeDescription });
      setTribeName("");
      setTribeDescription("");
    } catch (error) {
      console.error("Error creating tribe:", error);
    }
  };

  return (
    <div className="dark:bg-dbBackgroundColorDark dark:text-dbWhiteDark px-8 pt-4 pb-24">
      <LinkBackbtn linkTo="/admindashboard" content="Go back to Dashboard" />
      <PageInfo
        title="Create a new"
        secondTitle="Tribe"
        content="Fill out the form and click on the submit button to create a new official tribe in Danske Bank."
      />
      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <form className="w-1/2 px-8" onSubmit={handleSubmit}>
          <div className="flex flex-col">
            <label htmlFor="tribeName" className="text-md italic">
              Tribe Name
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="tribeName"
              placeholder="Name of the tribe"
              value={tribeName}
              onChange={(e) => setTribeName(e.target.value)}
              required
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="tribeDescription" className="text-md italic">
              Tribe Description
            </label>
            <textarea
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="tribeDescription"
              placeholder="Description of the tribe"
              value={tribeDescription}
              onChange={(e) => setTribeDescription(e.target.value)}
              required
            />
          </div>
          <button
            className="flex gap-2 px-5 py-2 mt-4 text-md transition ease-in-out rounded-full shadow-md text-dbSand bg-dbBrightBlue hover:bg-dbBrightBlueHover dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:hover:bg-dbLightBlue"
            type="submit"
            disabled={loading}
          >
            Create Tribe <IoCreateOutline />
          </button>
        </form>
      </Section>
      {loading && <p>Loading...</p>}
      {error && <p>Error: {error}</p>}
      {successMessage && (
        <p className="text-dbBlue dark:text-dbWhiteDark">
          Success: {successMessage}
        </p>
      )}
    </div>
  );
};

export default CreateTribe;
