import React from "react";
import Section from "../../Layouts/section/page";
import PageInfo from "../../Components/pageInfo/page";
import SearchField from "../../Components/searchField/page";
import PrimaryButton from "../../Components/Buttons/primaryBtn/page";
import FilterField from "../../Components/filterField/page";
import useFetchProducts from "../../Hooks/useFetchProducts";
import ProductCard from "../../Components/Cards/productCard/page";

const AllProducts: React.FC = () => {
  const {
    products,
    loading: productsLoading,
    error: productsError,
  } = useFetchProducts();

  return (
    <div className=" bg-dbBackgroundColor dark:bg-dbBackgroundColorDark">
      <PageInfo
        title="About"
        secondTitle="Our Products in Danske Bank"
        content="Embark on a journey through the diverse landscape of Products within Danske Bank! Here, innovation thrives, and initiatives take shape to transform ideas into reality. Explore a plethora of Products, each a testament to creativity, collaboration, and ingenuity. From cutting-edge technology ventures to transformative business endeavors, this is your gateway to the dynamic world of Products within Danske Bank."
      />

      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbCardBackgroundColorDark"
      >
        <div className="px-4">
          <div className="grid gap-5 pb-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            <SearchField />
            <FilterField />
          </div>
          <div className="grid gap-5 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {productsLoading ? (
              <p>Loading...</p>
            ) : productsError ? (
              <p className="text-center text-red-500">{productsError}</p>
            ) : (
              products.map((product) => (
                <ProductCard key={product.product_title} product={product} />
              ))
            )}
          </div>
          <div className="flex justify-center my-8">
            <PrimaryButton content="Show all products" linkTo="/products" />
          </div>
        </div>
      </Section>
      <Section
        bgColorLightMode="bg-dbBlue"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <div className="px-4">
          <h2 className="mb-8 font-medium text-center text-h2 text-dbSand ">
            Newly released products
          </h2>
          {/* Dynamisk indhold skal genereres her så den henter maksimalt de 4 nyeste projekter fra databasen */}
          <div className="grid flex-wrap justify-center grid-cols-1 gap-4 md:grid-cols-2 lg:grid-cols-4">
            {/* <ProjectCard
              bgColorLightMode="bg-dbWhite"
              bgColorDarkMode="bg-dbCardBackgroundColorDark"
            />
            <ProjectCard
              bgColorLightMode="bg-dbWhite"
              bgColorDarkMode="bg-dbCardBackgroundColorDark"
            />
            <ProjectCard
              bgColorLightMode="bg-dbWhite"
              bgColorDarkMode="bg-dbCardBackgroundColorDark"
            />
            <ProjectCard
              bgColorLightMode="bg-dbWhite"
              bgColorDarkMode="bg-dbCardBackgroundColorDark"
            /> */}
          </div>
        </div>
      </Section>
    </div>
  );
};

export default AllProducts;
