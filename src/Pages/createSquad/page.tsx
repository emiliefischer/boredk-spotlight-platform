import React, { useState, useEffect, ChangeEvent, FormEvent } from "react";
import useCreateSquad from "../../Hooks/useCreateSquad";
import PageInfo from "../../Components/pageInfo/page";
import Section from "../../Layouts/section/page";
import LinkBackbtn from "../../Components/Buttons/linkbackbtn/page";
import useFetchUsers from "../../Hooks/useFetchUsers";
import useFetchTribes from "../../Hooks/useFetchTribes";
import { IoCreateOutline } from "react-icons/io5";

const CreateSquad: React.FC = () => {
  const [formData, setFormData] = useState({
    squadName: "",
    squadDescription: "",
    tribeId: "",
    leaderUserId: "",
  });
  const { createSquad, loading, error, successMessage, resetSuccessMessage } =
    useCreateSquad();
  const { users, loading: usersLoading, error: usersError } = useFetchUsers();
  const {
    tribes,
    loading: tribesLoading,
    error: tribesError,
  } = useFetchTribes();

  useEffect(() => {
    if (successMessage) {
      console.log("Success Message:", successMessage);
      const timer = setTimeout(() => {
        resetSuccessMessage();
      }, 5000); // Clear success message after 5 seconds
      return () => clearTimeout(timer);
    }
  }, [successMessage, resetSuccessMessage]);

  const handleInputChange = (
    e: ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleTextareaChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    try {
      console.log("Submitting form...");
      console.log("Form Data:", formData);

      // Check if required fields are provided
      if (
        !formData.squadName ||
        !formData.squadDescription ||
        !formData.tribeId ||
        !formData.leaderUserId
      ) {
        throw new Error("Please provide all required fields.");
      }

      await createSquad(formData);
      setFormData({
        squadName: "",
        squadDescription: "",
        tribeId: "",
        leaderUserId: "",
      });
    } catch (error) {
      console.error("Error creating squad:", error);
    }
  };

  return (
    <div className="dark:bg-dbBackgroundColorDark dark:text-dbWhiteDark px-8 pt-4 pb-24">
      <LinkBackbtn linkTo="/admindashboard" content="Go back to Dashboard" />
      <PageInfo
        title="Create a new"
        secondTitle="Squad"
        content="Fill out the form and click on the submit button to create a new squad."
      />
      <Section
        bgColorLightMode="bg-dbBackgroundColor"
        bgColorDarkMode="bg-dbBackgroundColorDark"
      >
        <form className="w-1/2 px-8" onSubmit={handleSubmit}>
          <div className="flex flex-col">
            <label htmlFor="squadName" className="text-md italic">
              Squad Name
            </label>
            <input
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              type="text"
              id="squadName"
              name="squadName"
              placeholder="Name of the squad"
              value={formData.squadName}
              onChange={handleInputChange}
              required
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="squadDescription" className="text-md italic">
              Squad Description
            </label>
            <textarea
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="squadDescription"
              name="squadDescription"
              placeholder="Description of the squad"
              value={formData.squadDescription}
              onChange={handleTextareaChange}
              required
            />
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="tribeId" className="text-md italic">
              Tribe
            </label>
            <select
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="tribeId"
              name="tribeId"
              value={formData.tribeId}
              onChange={handleInputChange}
              required
            >
              <option value="">Select a tribe</option>
              {tribes.map((tribe) => (
                <option key={tribe.tribe_id} value={tribe.tribe_id}>
                  {tribe.tribe_name}
                </option>
              ))}
            </select>
          </div>
          <div className="flex flex-col mt-2">
            <label htmlFor="leaderUserId" className="text-md italic">
              Leader User
            </label>
            <select
              className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
              id="leaderUserId"
              name="leaderUserId"
              value={formData.leaderUserId}
              onChange={handleInputChange}
              required
            >
              <option value="">Select a leader</option>
              {users.map((user) => (
                <option key={user.user_id} value={user.user_id}>
                  {user.user_first_name} {user.user_last_name}
                </option>
              ))}
            </select>
          </div>
          <button
            className="flex gap-2 px-5 py-2 mt-4 text-md transition ease-in-out rounded-full shadow-md text-dbSand bg-dbBrightBlue hover:bg-dbBrightBlueHover dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:hover:bg-dbLightBlue"
            type="submit"
            disabled={loading}
          >
            Create Squad <IoCreateOutline />
          </button>
        </form>
      </Section>
      {loading && <p>Loading...</p>}
      {error && <p>Error: {error}</p>}
      {successMessage && (
        <p className="text-dbBlue dark:text-dbWhiteDark">
          Success: {successMessage}
        </p>
      )}
    </div>
  );
};

export default CreateSquad;
