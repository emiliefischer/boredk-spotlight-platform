import React from "react";

type ProductLinkProps = {
  image: string;
  link: string;
};

const ProductLinkCard = ({ image, link }: ProductLinkProps) => {
  return (
    <div className="flex flex-col items-center text-dbBlue dark:text-dbWhite">
      <img src={image} alt="link_picture" className="w-12 mx-auto my-4" />
      <a href="#">{link}</a>
    </div>
  );
};

export default ProductLinkCard;
