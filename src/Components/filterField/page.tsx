import React from "react";

const FilterField = () => {
  return (
    <input
      className="w-full px-5 py-2 text-sm border-none rounded-full shadow-md lg:col-start-4 search-field bg-dbSand focus:ring-dbGrey"
      type="text"
      placeholder="Filter"
    />
  );
};

export default FilterField;
