import React from "react";
import { FaArrowRightLong } from "react-icons/fa6";

type LinkBtnProps = {
  content: string;
  linkTo: string;
};

const Linkbtn = ({ content, linkTo }: LinkBtnProps) => {
  return (
    <div>
      <a
        className="flex gap-2 text-sm items-center text-dbBrightBlue dark:text-dbBrightBlueDark"
        href={linkTo}
      >
        {content}
        <FaArrowRightLong />
      </a>
    </div>
  );
};

export default Linkbtn;
