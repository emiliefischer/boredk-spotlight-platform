import React from "react";
import { FaArrowLeftLong } from "react-icons/fa6";

type LinkBackBtnProps = {
  content: string;
  linkTo: string;
};

const LinkBackbtn = ({ content, linkTo }: LinkBackBtnProps) => {
  return (
    <div>
      <a
        className="flex gap-2 text-sm items-center text-dbBrightBlue dark:text-dbBrightBlueDark"
        href={linkTo}
      >
        <FaArrowLeftLong />
        {content}
      </a>
    </div>
  );
};

export default LinkBackbtn;
