import axios from "axios";
import React, { useEffect, useState } from "react";
import useCountProducts from "../../Hooks/useCountProduct";
import useCountProductsInProgress from "../../Hooks/useCountProductsInProgress";
import useCountProductsNew from "../../Hooks/useCountProductsNew";
import useCountProductsClosed from "../../Hooks/useCountProductsClosed";

// Definér en type for props
type StatBlockProps = {
  number: string | number;
  label: string;
  bgColor: string;
  bgColorDark: string;
};

// Definér StatBlock-komponenten
const StatBlock = ({ number, label, bgColor, bgColorDark }: StatBlockProps) => (
  <div className="flex flex-col items-center justify-center px-12">
    <h1 className="mb-4 font-semibold text-number text-dbWhite dark:text-dbWhiteDark">
      {number}
    </h1>
    <div className={`px-4 py-1 rounded-sm dark:${bgColorDark} ${bgColor}`}>
      <p className="text-dbWhite dark:text-dbCardBackgroundColorDark">
        {label}
      </p>
    </div>
  </div>
);

// Definér StatisticsOverview-komponenten
const StatisticsOverview = () => {
  const { productCount, loading: productLoading } = useCountProducts();
  const { productsInProgressCount, loading: productsInProgressLoading } =
    useCountProductsInProgress();
  const { productsNewCount, loading: productsNewLoading } =
    useCountProductsNew();
  const { productsClosedCount, loading: productsClosedLoading } =
    useCountProductsClosed();

  if (
    productLoading ||
    productsInProgressLoading ||
    productsNewLoading ||
    productsClosedLoading
  ) {
    return <div>Loading...</div>;
  }

  return (
    <div className="grid sm:grid-cols-2 lg:grid-cols-4 bg-dbBlue dark:bg-dbCardBackgroundColorDark dark:text-dbWhiteDark">
      <StatBlock
        number={productCount || "Loading..."}
        label="Projects"
        bgColor="bg-dbBrightBlue"
        bgColorDark="bg-dbBrightBlueDark"
      />
      <StatBlock
        number={productsInProgressCount || "Loading..."}
        label="In progress"
        bgColor="bg-dbLabelPink"
        bgColorDark="bg-dbLabelPinkDark"
      />
      <StatBlock
        number={productsNewCount || "Loading..."}
        label="New"
        bgColor="bg-dbLabelGreen"
        bgColorDark="bg-dbLabelGreenDark"
      />
      <StatBlock
        number={productsClosedCount || "Loading..."}
        label="Closed"
        bgColor="bg-dbLabelOrange"
        bgColorDark="bg-dbLabelOrangeDark"
      />
    </div>
  );
};

export default StatisticsOverview;
