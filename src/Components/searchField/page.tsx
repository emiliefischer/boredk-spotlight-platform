import React from "react";

const SearchField = () => {
  return (
    <input
      className="w-full px-5 py-2 text-sm border-none rounded-full shadow-md search-field bg-dbSand dark:bg-dbSearchDark dark:placeholder-dbWhiteDark dark:text-dbWhite focus:ring-dbGrey"
      type="text"
      placeholder="Search"
    />
  );
};

export default SearchField;
