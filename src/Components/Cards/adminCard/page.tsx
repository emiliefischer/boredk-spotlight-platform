import React from "react";

// Define the AdminCardProps interface
type AdminCardProps = {
  title: string;
  number: string;
  isSelected: boolean;
  onSelect: () => void;
};

// Define the AdminCard component
const AdminCard: React.FC<AdminCardProps> = ({
  title,
  number,
  isSelected,
  onSelect,
}) => {
  return (
    <a
      href="#"
      onClick={onSelect}
      className={`flex flex-col p-4 space-y-2 rounded-sm ${
        isSelected ? "bg-dbBlue" : "bg-dbLightBlue"
      } dark:bg-dbNavBackgroundColorDark`}
    >
      <div className="flex items-center justify-between">
        <p
          className={`text-base font-medium ${
            isSelected ? "text-white" : "dark:text-dbWhite"
          }`}
        >
          {title}
        </p>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="32"
          height="32"
          viewBox="0 0 256 256"
        >
          <path
            fill="#007BC7"
            d="M128 24a104 104 0 1 0 104 104A104.11 104.11 0 0 0 128 24m40 120a8 8 0 0 1-16 0v-28.69l-50.34 50.35a8 8 0 0 1-11.32-11.32L140.69 104H112a8 8 0 0 1 0-16h48a8 8 0 0 1 8 8Z"
          />
        </svg>
      </div>
      <p
        className={`font-semibold text-h2 ${
          isSelected ? "text-white" : "text-dbBlue"
        } dark:text-dbSand`}
      >
        {number}
      </p>
    </a>
  );
};

// Export the AdminCard component
export default AdminCard;
