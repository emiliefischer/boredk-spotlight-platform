import React from "react";
import mail from "../../../Assets/Images/mail.svg";
import { MdOutlineMail } from "react-icons/md";

const SquadMemberCard = () => {
  return (
    <div className="py-6 rounded-sm bg-dbWhite dark:bg-dbNavBackgroundColorDark px-7">
      <p className="pb-5 text-md text-dbBlue dark:text-dbWhiteDark">
        Squad role
      </p>
      <p className="pb-5 text-sm text-dbBlue dark:text-dbWhiteDark">
        Name: Name, Lastname
      </p>
      <div className="flex gap-2 px-5 py-4 rounded-full bg-dbSand dark:bg-dbBoxShadeOneDark">
        <MdOutlineMail className="fill-dbBrightBlue dark:fill-dbBrightBlueDark" />
        <a
          className="self-center text-xs text-dbBrightBlue dark:text-dbLightBlueDark"
          href="#"
        >
          email@danskebank.dk
        </a>
      </div>
    </div>
  );
};

export default SquadMemberCard;
