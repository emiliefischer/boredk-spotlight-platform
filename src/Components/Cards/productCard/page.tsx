import React from "react";
import projectImg from "../../../Assets/Images/projectImg.png";
import { FaCircleArrowUp } from "react-icons/fa6";
import { Product } from "../../../Types/productInterface";
import { getStatusColorClass } from "../../../Utils/statusUtils";
import { Link } from "react-router-dom";

interface ProductCardProps {
  product: Product;
}

const ProductCard: React.FC<ProductCardProps> = ({ product }) => {
  const statusColorClass = getStatusColorClass(product.status_name);

  return (
    <Link
      to={`/products/${encodeURIComponent(
        product.product_title.toLowerCase().replace(/\s+/g, "-")
      )}`}
    >
      <div className="flex flex-col h-full p-4 space-y-2 rounded-sm bg-dbSand dark:bg-dbCardBackgroundColorDark">
        <div className="flex items-center justify-between">
          <div className={`px-4 py-0 rounded-sm ${statusColorClass}`}>
            <p className="text-sm text-dbWhite dark:text-dbCardBackgroundColorDark">
              {product.status_name}
            </p>
          </div>
          <FaCircleArrowUp className="w-5 h-6 mr-1 rotate-45 fill-dbBrightBlue dark:fill-dbBrightBlueDark" />
        </div>
        <img src={projectImg} alt="Logo" />
        <h3 className="font-semibold text-h4 text-dbBlue dark:text-dbWhiteDark">
          {product.product_title}
        </h3>
        <h4 className="font-semibold text-h5 text-dbBlue dark:text-dbWhiteDark">
          <strong>PO:</strong> {product.product_owner_name}
        </h4>
        <p className="text-md text-dbBlue dark:text-dbWhiteDark">
          {product.product_description}
        </p>
      </div>
    </Link>
  );
};

export default ProductCard;
