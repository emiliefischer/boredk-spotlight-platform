import React from "react";
// import arrowright from "../../../Assets/Images/arrowright.svg";
import SecondaryButton from "../../Buttons/secondaryBtn/page";

type SectionProps = {
  title: string;
  content: string;
  link: string;
  bgColorLight: string;
  bgColorDark: string;
};

const InfoLinkBoxCard = ({
  bgColorLight,
  bgColorDark,
  title,
  content,
  link,
}: SectionProps) => (
  <div
    className={`flex flex-col p-4 space-y-2 gap-4 rounded-sm ${bgColorLight} dark:${bgColorDark}`}
  >
    <h3 className="font-semibold text-h3 text-dbBlue dark:text-dbWhiteDark">
      {title}
    </h3>
    <p className="text-base text-dbBlue dark:text-dbWhiteDark">{content}</p>
    <SecondaryButton content="Go to" linkTo={link} />
  </div>
);

export default InfoLinkBoxCard;
