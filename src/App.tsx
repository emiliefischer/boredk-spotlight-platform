import React from "react";
import "flowbite";
import { Routes, Route } from "react-router-dom";
import AllTribes from "./Pages/tribes/page";
import AllSquads from "./Pages/squads/page";
import Home from "./Pages/LandingPage/page";
import RootLayout from "./Layouts/rootLayout/page";
import Products from "./Pages/products/page";
import LoginModalPopup from "./Components/loginModal/page";
import SingleTribe from "./Pages/tribes/[id]/page";
import SingleSquad from "./Pages/squads/[id]/page";
import SingleProduct from "./Pages/products/[id]/page";
import AdminDashboard from "./Pages/adminDashboard/page";
import CreateUser from "./Pages/createUser/page";
import CreateUserForm from "./Pages/createUser/page";
import CreateTribe from "./Pages/createTribe/page";
import CreateSquad from "./Pages/createSquad/page";
import CreateProduct from "./Pages/createProduct/page";
// import UserProfile from "./Pages/userProfile/[id]/page";
import UpdateTribe from "./Pages/updateTribe/page";

function App() {
  return (
    <RootLayout>
      <LoginModalPopup />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/products" element={<Products />} />
        <Route path="/tribes" element={<AllTribes />} />
        <Route path="/squads" element={<AllSquads />} />
        <Route path="/tribes/:tribeName" element={<SingleTribe />} />
        <Route path="/squads/:squadName" element={<SingleSquad />} />
        <Route path="/products/:productTitle" element={<SingleProduct />} />
        <Route path="/admindashboard" element={<AdminDashboard />} />
        <Route path="/create-user" element={<CreateUserForm />} />
        {/* <Route path="/userprofile" element={<UserProfile />} /> */}
        <Route path="/create-tribe" element={<CreateTribe />} />
        <Route path="/create-squad" element={<CreateSquad />} />
        <Route path="/create-product" element={<CreateProduct />} />
        <Route path="/update-tribe/:tribeId" element={<UpdateTribe />} />
      </Routes>
    </RootLayout>
  );
}

export default App;
