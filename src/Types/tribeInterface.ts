export interface Tribe {
  tribe_id: string;
  tribe_name: string;
  tribe_describtion: string;
  created_at: string;
  tribe_leaders?: string;
  total_squads: string;
}
