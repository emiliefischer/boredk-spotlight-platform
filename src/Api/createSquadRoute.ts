import express, { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid"; // Import UUID generation function
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// Create a new squad
router.post("/", async (req: Request, res: Response) => {
  try {
    const { squadName, squadDescription, tribeId, leaderUserId } = req.body;

    // Generate UUID without dashes
    const squadId = uuidv4().replace(/-/g, "");

    // Check if required fields are provided
    if (!squadName || !squadDescription || !tribeId || !leaderUserId) {
      return res.status(400).json({ error: "Missing required fields" });
    }

    // Example query to insert a new squad into the database
    const query = `
      INSERT INTO squads (squad_id, squad_created_at, squad_updated_at, squad_name, squad_describtion, squad_tribe_fk, squad_lead_user_fk)
      VALUES (?, ?, ?, ?, ?, ?, ?)
    `;

    // Execute the query
    await executeQuery(query, [
      squadId,
      new Date(),
      new Date(),
      squadName,
      squadDescription,
      tribeId,
      leaderUserId,
    ]);

    // Respond with success message
    res.status(201).json({ message: "Squad created successfully" });
  } catch (error) {
    console.error("Error creating squad:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
