import express from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// GET all product statuses
router.get("/", async (req, res) => {
  try {
    // Query to fetch all product statuses from the database
    const query = "SELECT * FROM product_status";

    // Execute the query
    const statuses = await executeQuery(query);

    // Respond with the fetched statuses
    res.status(200).json(statuses);
  } catch (error) {
    console.error("Error fetching product statuses:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
