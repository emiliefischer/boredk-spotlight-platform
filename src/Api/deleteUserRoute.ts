import express from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

router.delete("/:userId", async (req, res) => {
  try {
    const { userId } = req.params;

    const query = `DELETE FROM users WHERE user_id = ?`;

    await executeQuery(query, [userId]);

    res.status(200).json({ message: "User deleted successfully" });
  } catch (error) {
    console.error("Error deleting user:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
