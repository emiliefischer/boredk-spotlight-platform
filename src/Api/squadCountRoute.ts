const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get count of all squads
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching squad count from database...");
    const query = `SELECT COUNT(*) AS squadCount FROM squads`;
    const result = await executeQuery(query);
    const squadCount = result[0].squadCount;
    console.log("Squad count fetched:", squadCount);
    res.json({ squadCount });
  } catch (error) {
    console.error("Error retrieving squad count:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
