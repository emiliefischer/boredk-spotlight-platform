import express, { Request, Response } from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// Update a tribe
router.put("/:tribeId", async (req: Request, res: Response) => {
  try {
    const { tribeId } = req.params;
    const { tribeName, tribeDescription } = req.body;

    // Construct the SET clause of the SQL query dynamically based on provided fields
    let setClause = "";
    const params = [];

    if (tribeName) {
      setClause += "tribe_name = ?, ";
      params.push(tribeName);
    }

    if (tribeDescription) {
      setClause += "tribe_describtion = ?, ";
      params.push(tribeDescription);
    }

    // Remove trailing comma and space from setClause
    setClause = setClause.replace(/, $/, "");

    // Check if at least one field is provided
    if (!setClause) {
      return res
        .status(400)
        .json({ error: "At least one field is required for update" });
    }

    // Example query to update a tribe in the database
    const query = `
      UPDATE tribes
      SET ${setClause}
      WHERE tribe_id = ?
    `;

    // Add tribeId to params array
    params.push(tribeId);

    // Execute the query
    await executeQuery(query, params);

    // Respond with success message
    res.status(200).json({ message: "Tribe updated successfully" });
  } catch (error) {
    console.error("Error updating tribe:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
