const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

router.get("/:squadName", async (req: Request, res: Response) => {
  const { squadName } = req.params;
  try {
    console.log(`Fetching squad with name: ${squadName}`);
    const query = `
      SELECT * FROM squads WHERE LOWER(REPLACE(squad_name, ' ', '-')) = ?
    `;
    const squads = await executeQuery(query, [squadName]);
    if (squads.length > 0) {
      console.log("Squad fetched:", squads[0]);
      res.json(squads[0]);
    } else {
      res.status(404).json({ error: "Squad not found" });
    }
  } catch (error) {
    console.error("Error retrieving squad:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
