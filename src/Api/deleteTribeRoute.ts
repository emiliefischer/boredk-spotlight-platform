import express from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// DELETE a tribe
router.delete("/:tribeId", async (req, res) => {
  try {
    const { tribeId } = req.params;

    // Example query to delete a tribe from the database
    const query = `
      DELETE FROM tribes WHERE tribe_id = ?
    `;

    // Execute the query
    await executeQuery(query, [tribeId]);

    // Respond with success message
    res.status(200).json({ message: "Tribe deleted successfully" });
  } catch (error) {
    console.error("Error deleting tribe:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
