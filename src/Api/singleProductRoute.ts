// src/Api/singleProductRoute.ts

const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

router.get("/:productTitle", async (req: Request, res: Response) => {
  const { productTitle } = req.params;
  try {
    console.log(`Fetching product with title: ${productTitle}`);
    const query = `
      SELECT 
        p.*, 
        ps.status_name,
        CONCAT(u.user_first_name, ' ', u.user_last_name) AS product_owner_name
      FROM 
        products p
      JOIN 
        product_status ps ON p.product_status_fk = ps.status_id
      LEFT JOIN 
        users u ON p.product_productowner_fk = u.user_id
      WHERE 
        LOWER(REPLACE(p.product_title, ' ', '-')) = ?
    `;
    const products = await executeQuery(query, [productTitle]);
    if (products.length > 0) {
      console.log("Product fetched:", products[0]);
      res.json(products[0]);
    } else {
      res.status(404).json({ error: "Product not found" });
    }
  } catch (error) {
    console.error("Error retrieving product:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
