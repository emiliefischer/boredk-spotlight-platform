import express from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// DELETE a squad
router.delete("/:squadId", async (req, res) => {
  try {
    const { squadId } = req.params;

    // Example query to delete a squad from the database
    const query = `
      DELETE FROM squads WHERE squad_id = ?
    `;

    // Execute the query
    await executeQuery(query, [squadId]);

    // Respond with success message
    res.status(200).json({ message: "Squad deleted successfully" });
  } catch (error) {
    console.error("Error deleting squad:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
