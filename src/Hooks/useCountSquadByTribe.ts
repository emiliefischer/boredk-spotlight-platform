import { useEffect, useState } from "react";
import axios from "axios";

const useCountSquadsByTribe = (tribeId: string) => {
  const [squadCount, setSquadCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchSquadCount = async () => {
      try {
        const response = await axios.get(
          `http://localhost:4000/api/squadCountByTribe/${tribeId}`
        );
        setSquadCount(response.data.squadCount);
      } catch (err) {
        setError("Error fetching squad count");
      } finally {
        setLoading(false);
      }
    };

    fetchSquadCount();
  }, [tribeId]);

  return { squadCount, loading, error };
};

export default useCountSquadsByTribe;
