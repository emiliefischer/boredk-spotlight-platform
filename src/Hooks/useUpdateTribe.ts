import axios, { AxiosError } from "axios";
import { useState } from "react";

interface UpdateTribeResponse {
  message: string;
}

interface UpdateTribeError {
  message: string;
}

const useUpdateTribe = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [successMessage, setSuccessMessage] = useState<string | null>(null);

  const updateTribe = async (tribeId: string, updatedData: any) => {
    setLoading(true);
    setError(null);
    setSuccessMessage(null);

    try {
      const response = await axios.put<UpdateTribeResponse>(
        `http://localhost:4000/api/updateTribe/:tribeId`,
        updatedData
      );
      setSuccessMessage(response.data.message);
    } catch (error) {
      handleAxiosError(error);
    } finally {
      setLoading(false);
    }
  };

  const handleAxiosError = (error: any) => {
    if (axios.isAxiosError(error)) {
      const axiosError = error as AxiosError<UpdateTribeError>;
      if (axiosError.response && axiosError.response.data) {
        setError(axiosError.response.data.message);
      } else {
        setError("An error occurred while processing your request");
      }
    } else {
      setError("An error occurred while processing your request");
    }
  };

  const resetSuccessMessage = () => {
    setSuccessMessage(null);
  };

  return { updateTribe, loading, error, successMessage, resetSuccessMessage };
};

export default useUpdateTribe;
