import { useState, useEffect } from "react";
import axios from "axios";
import { User } from "../Types/userInterface";

const useFetchUsers = (limit?: number) => {
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get<User[]>(
          "http://localhost:4000/api/usersData"
        );
        let sortedUsers = response.data.sort(
          (a, b) =>
            new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        );
        if (limit) {
          sortedUsers = sortedUsers.slice(0, limit);
        }
        setUsers(sortedUsers);
      } catch (error) {
        console.error("Error fetching users:", error);
        setError("Error fetching users");
      } finally {
        setLoading(false);
      }
    };

    fetchUsers();
  }, [limit]);

  return { users, loading, error };
};

export default useFetchUsers;
