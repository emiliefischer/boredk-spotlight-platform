import axios from "axios";
import { useEffect, useState } from "react";

const useCountProductsInProgress = () => {
  const [productsInProgressCount, setProductsInProgressCount] = useState<
    number | null
  >(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductsInProgressCount = async () => {
      try {
        const response = await axios.get<{ productsInProgressCount: number }>(
          "http://localhost:4000/api/products/productsInProgressCount"
        );
        setProductsInProgressCount(response.data.productsInProgressCount);
        setLoading(false); // Set loading to false after successful fetch
      } catch (error) {
        console.error("Error fetching products in progress count:", error);
        setLoading(false); // Set loading to false in case of error
      }
    };

    fetchProductsInProgressCount();
  }, []);

  return { productsInProgressCount, loading };
};

export default useCountProductsInProgress;
