import { useState, useEffect } from "react";
import axios from "axios";
import { Product } from "../Types/productInterface";

const useFetchProducts = (limit?: number) => {
  const [products, setProducts] = useState<Product[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const response = await axios.get<Product[]>(
          "http://localhost:4000/api/productsData"
        );
        let sortedProducts = response.data.sort(
          (a, b) =>
            new Date(b.product_created_at).getTime() -
            new Date(a.product_created_at).getTime()
        );
        if (limit) {
          sortedProducts = sortedProducts.slice(0, limit);
        }
        setProducts(sortedProducts);
      } catch (error) {
        console.error("Error fetching products:", error);
        setError("Error fetching products");
      } finally {
        setLoading(false);
      }
    };

    fetchProducts();
  }, [limit]);

  return { products, loading, error };
};

export default useFetchProducts;
