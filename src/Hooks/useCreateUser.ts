import axios, { AxiosError } from "axios";
import { useEffect, useState } from "react";

interface CreateUserResponse {
  message: string;
}

const useCreateUser = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const createUser = async (formData: FormData) => {
    setLoading(true);
    setError(null);
    try {
      // Make API request to create user
      const response = await axios.post<CreateUserResponse>(
        "http://localhost:4000/api/createUser",
        formData
      );
      setLoading(false);
      return response.data.message; // Return success message
    } catch (error) {
      setLoading(false);
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response && axiosError.response.data) {
          const responseData = axiosError.response.data as { error: string };
          setError(responseData.error); // Set error message from response
        } else {
          setError("An error occurred while processing your request");
        }
      } else {
        setError("An error occurred while processing your request");
      }
      throw error; // Re-throw the error for the component to catch
    }
  };

  return { createUser, loading, error };
};

export default useCreateUser;
