import axios from "axios";
import { useEffect, useState } from "react";

const useCountSquads = () => {
  const [squadCount, setSquadCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchSquadCount = async () => {
      try {
        const response = await axios.get<{ squadCount: number }>(
          "http://localhost:4000/api/squads/squadCount"
        );
        setSquadCount(response.data.squadCount);
        setLoading(false); // Set loading to false after successful fetch
      } catch (error) {
        console.error("Error fetching squad count:", error);
        setLoading(false); // Set loading to false in case of error
      }
    };

    fetchSquadCount();
  }, []);
  return { squadCount, loading };
};

export default useCountSquads;
