import axios from "axios";
import { useEffect, useState } from "react";

const useCountTribes = () => {
  const [tribeCount, setTribeCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductCount = async () => {
      try {
        const response = await axios.get<{ tribeCount: number }>(
          "http://localhost:4000/api/tribes/tribeCount"
        );
        setTribeCount(response.data.tribeCount);
        setLoading(false); // Set loading to false after successful fetch
      } catch (error) {
        console.error("Error fetching product count:", error);
        setLoading(false); // Set loading to false in case of error
      }
    };

    fetchProductCount();
  }, []);
  return { tribeCount, loading };
};

export default useCountTribes;
