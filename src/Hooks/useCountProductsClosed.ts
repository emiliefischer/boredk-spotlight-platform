import axios from "axios";
import { useEffect, useState } from "react";

const useCountProductsClosed = () => {
  const [productsClosedCount, setProductsClosedCount] = useState<number | null>(
    null
  );
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductClosedCount = async () => {
      try {
        const response = await axios.get<{ productsClosedCount: number }>(
          "http://localhost:4000/api/products/productsClosedCount"
        );
        setProductsClosedCount(response.data.productsClosedCount);
        setLoading(false); // Set loading to false after successful fetch
      } catch (error) {
        console.error("Error fetching closed products count:", error);
        setLoading(false); // Set loading to false in case of error
      }
    };

    fetchProductClosedCount();
  }, []);

  return { productsClosedCount, loading };
};

export default useCountProductsClosed;
