// useRequireAdmin.ts
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const useRequireAdmin = (userRoles: any) => {
  const navigate = useNavigate();

  useEffect(() => {
    if (!userRoles || Object.keys(userRoles).length === 0) {
      // User not logged in
      navigate("/");
    } else if (
      userRoles.isSuperiorAdmin !== 1 ||
      userRoles.isSquadAdmin !== 1 ||
      userRoles.isTribeAdmin !== 1
    ) {
      // User logged in but not an admin
      navigate("/admindashboard");
    }
  }, [userRoles, navigate]);
};

export default useRequireAdmin;
