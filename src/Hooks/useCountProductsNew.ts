import axios from "axios";
import { useEffect, useState } from "react";

const useCountProductsNew = () => {
  const [productsNewCount, setProductsNewCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductNewCount = async () => {
      try {
        const response = await axios.get<{ productsNewCount: number }>(
          "http://localhost:4000/api/products/productsNewCount"
        );
        setProductsNewCount(response.data.productsNewCount);
        setLoading(false); // Set loading to false after successful fetch
      } catch (error) {
        console.error("Error fetching products in progress count:", error);
        setLoading(false); // Set loading to false in case of error
      }
    };

    fetchProductNewCount();
  }, []);

  return { productsNewCount, loading };
};

export default useCountProductsNew;
