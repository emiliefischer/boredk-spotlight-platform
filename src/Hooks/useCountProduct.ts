import axios from "axios";
import { useEffect, useState } from "react";

const useCountProducts = () => {
  const [productCount, setProductCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductCount = async () => {
      try {
        const response = await axios.get<{ productCount: number }>(
          "http://localhost:4000/api/products/productCount"
        );
        setProductCount(response.data.productCount);
        setLoading(false); // Set loading to false after successful fetch
      } catch (error) {
        console.error("Error fetching product count:", error);
        setLoading(false); // Set loading to false in case of error
      }
    };

    fetchProductCount();
  }, []);
  return { productCount, loading };
};

export default useCountProducts;
