import { useState, useEffect } from "react";
import axios from "axios";

const useFetchRoles = () => {
  const [roles, setRoles] = useState<any[]>([]); // Change 'any[]' to match your role type
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchRoles = async () => {
      try {
        const response = await axios.get<any[]>(
          "http://localhost:4000/api/rolesData" // Replace with your roles API endpoint
        );
        setRoles(response.data);
      } catch (error) {
        console.error("Error fetching roles:", error);
        setError("Error fetching roles");
      } finally {
        setLoading(false);
      }
    };

    fetchRoles();
  }, []);

  return { roles, loading, error };
};

export default useFetchRoles;
