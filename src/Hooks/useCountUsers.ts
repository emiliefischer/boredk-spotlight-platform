import axios from "axios";
import { useEffect, useState } from "react";

const useCountUsers = () => {
  const [userCount, setUserCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchUserCount = async () => {
      try {
        const response = await axios.get<{ userCount: number }>(
          "http://localhost:4000/api/users/userCount"
        );
        setUserCount(response.data.userCount);
        setLoading(false); // Set loading to false after successful fetch
      } catch (error) {
        console.error("Error fetching user count:", error);
        setLoading(false); // Set loading to false in case of error
      }
    };

    fetchUserCount();
  }, []);
  return { userCount, loading };
};

export default useCountUsers;
