import React from "react";
// import { StringMappingType } from "typescript";

type SectionProps = {
  children: React.ReactNode;
  bgColorLightMode: string;
  bgColorDarkMode: string;
};

const Section = ({
  children,
  bgColorLightMode,
  bgColorDarkMode,
}: SectionProps) => {
  return (
    <section className={`py-12 ${bgColorLightMode} dark:${bgColorDarkMode}`}>
      {children}
    </section>
  );
};

export default Section;
