import React from "react";
import ToggleDarkMode from "../../Components/toggleDarkMode/page";
import { Flowbite } from "flowbite-react";
import SideNavigationBar from "../sideBar/page";
import NavBarNotLogged from "../navBarNotLogged/page";
import NavBarLogged from "../navBarLogged/page";
import useAuth from "../../Hooks/useAuthHook";

type RootLayoutProps = {
  children: React.ReactNode;
};

const RootLayout = ({ children }: RootLayoutProps) => {
  const loggedIn = useAuth();

  return (
    <Flowbite>
      <div>
        {/* Toggle Function */}
        <ToggleDarkMode />
        {/* <!-- Navigation --> */}
        <div className="flex h-screen bg-dbBackgroundColor">
          {/* <!-- sidebar --> */}
          <SideNavigationBar />
          <div className="flex flex-col flex-1 overflow-y-auto">
            {/* <!-- Small header with searchfield --> */}
            {loggedIn ? <NavBarLogged /> : <NavBarNotLogged />}
            {/* Dette er hovedindholdsområdet, hvor children skal placeres */}
            <div className="">{children}</div>
          </div>
        </div>
      </div>
    </Flowbite>
  );
};

export default RootLayout;
