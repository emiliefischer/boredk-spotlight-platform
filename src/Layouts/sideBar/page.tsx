import React from "react";
import dbLogo from "../../Assets/Images/logo.png";
import useAuth from "../../Hooks/useAuthHook";
import LogoutButton from "../../Components/Buttons/logoutBtn/page";

const SideBar = () => {
  const loggedIn = useAuth();

  return (
    <div className="flex flex-col w-64 border-r border-dbGrey dark:border-none md:flex bg-dbWhite dark:bg-dbNavBackgroundColorDark">
      <a href="/" className="flex items-center justify-center h-16 p-4">
        <img
          src={dbLogo}
          alt="Logo"
          className="border border-1 border-dbBlue"
        />
      </a>
      <div className="flex flex-col justify-between flex-1 overflow-y-auto">
        <nav className="px-2 py-4">
          <a
            href="/tribes"
            className="flex items-center px-4 py-2 rounded-sm hover:ease-in-out hover:transition text-dbBlue hover:bg-dbSand dark:text-dbWhiteDark dark:hover:bg-dbBackgroundColorDark"
          >
            Tribes
          </a>
          <a
            href="/squads"
            className="flex items-center px-4 py-2 mt-2 rounded-sm text-dbBlue hover:bg-dbSand hover:ease-in-out hover:transition dark:text-dbWhiteDark dark:hover:bg-dbBackgroundColorDark"
          >
            Squads
          </a>
          <a
            href="/products"
            className="flex items-center px-4 py-2 mt-2 rounded-sm text-dbBlue hover:bg-dbSand hover:ease-in-out hover:transition dark:text-dbWhiteDark dark:hover:bg-dbBackgroundColorDark"
          >
            Products
          </a>
        </nav>
        {loggedIn ? (
          <div className="p-4 mb-10">
            <LogoutButton />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default SideBar;
